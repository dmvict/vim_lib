" Date Create: 2015-03-04 11:34:58
" Last Change: see date in git
" Author: Artur Sh. Mamedbekov (Artur-Mamedbekov@yandex.ru)
" License: GNU GPL v3 (http://www.gnu.org/copyleft/gpl.html)
" Contributor: dmvict ( dm.vict.kr@gmail.com )

let s:Path = vim_lib#base#Path#
let s:File = vim_lib#base#File#
let s:System = vim_lib#sys#System#

""
" @var { String } Current loading level.
""

let vim_lib#sys#Autoload#currentLevel = ''

""
" @var { Dictionary } A dictionary for loading levels. The dictionary schema: { [level]: { plugdir : [plugins directory], plugins: [pluginName1, ...] }, ...}.
""

let vim_lib#sys#Autoload#levels = {}

" Setup runtimepath

set nocompatible
set exrc
let s:rtp = [ $VIMRUNTIME, s:Path.join( $VIM, 'vimfiles' ) ]
if s:System.isWindows()
    call add( s:rtp, $HOME . s:Path.slash . 'vimfiles' )
else
    if has( 'nvim' )
        call add( s:rtp, $HOME . s:Path.slash . '.config' . s:Path.slash . 'nvim' )
    else
        call add( s:rtp, $HOME . s:Path.slash . '.vim' )
    endif
endif
call add( s:rtp, '.vim' )
let &rtp = join( s:rtp, ',' )

"//

""
" Command `Plugin` loads the defined plugin from argument for current loading level.
""

com! -nargs=+ Plugin call vim_lib#sys#Autoload#plugin( <args> )

"//

""
" Method initializes loading queue for current level.
"
" @param { String } level - Path to directory that contains loading files ( on system level it is $VIMRUNTIME, on user level it's ~/.vim or ~/vimfiles, on project level it's .vim ).
" @param { String } plugindir - Name of directory that contains editor plugins ( usually it named `bundle` ). This directory should be child of directory defined in argument {-level-}.
""

function! vim_lib#sys#Autoload#init( level, plugindir )
    let g:vim_lib#sys#Autoload#levels[ a:level ] = { 'plugdir' : a:plugindir, 'plugins' : [] }
    let g:vim_lib#sys#Autoload#currentLevel = a:level
endfunction

"//

""
" Method plugin() adds passed plugin with name {-name-} to loading queue.
"
" @param { String } name - Name of the plugin.
" @param { Dictionary } options - A dictionary of global options. Options will be settled before plugin loading. Options will have form : g:pluginName#key = value.
""

function! vim_lib#sys#Autoload#plugin( name, ... )
    let l:level = g:vim_lib#sys#Autoload#currentLevel
    let l:plugdir = g:vim_lib#sys#Autoload#levels[ l:level ][ 'plugdir' ]
    let l:plug = s:File.absolute( s:Path.join( l:level, l:plugdir, a:name ) )
    if l:plug.isDir()
        call add( g:vim_lib#sys#Autoload#levels[ l:level ][ 'plugins' ], a:name )
        let &rtp =  l:plug.getAddress() . ',' . &rtp
        let &rtp =  &rtp . ',' . l:plug.getAddress() . '/after'
        if exists( 'a:1' )
            for l:prop in keys( a:1 )
                let g:[ a:name. '#' . l:prop ] = a:1[ l:prop ]
            endfor
        endif
    else
        echo 'Plugin "' . a:name . '" not found. Please, install plugin or change plugin name.'
    endif
endfunction

"//

""
" Method getLevels() returns info about used loading levels.
"
" @return { Dictionary } - Returns value vim_lib#sys#Autoload#levels.
""

function! vim_lib#sys#Autoload#getLevels()
    return g:vim_lib#sys#Autoload#levels
endfunction

"//

""
" Method isPlug() checks that plugin {-name-} is used.
" Method returns true event if plugin {-name-} is not installed but loaded by command `Plugin`.
"
" @param { String } name - Name of plugin.
"
" @returns { Boolean } Returns true if plugin is used, otherwise returns false.
""

function! vim_lib#sys#Autoload#isPlug( name )
    for l:body in values( g:vim_lib#sys#Autoload#levels )
        if index( l:body.plugins, a:name ) != -1
            return 1
        endif
    endfor
    return 0
endfunction
