" Date Create: 2015-01-07 16:18:33
" Last Change: see date in git
" Author: Artur Sh. Mamedbekov ( Artur-Mamedbekov@yandex.ru )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )
" Contributor: dmvict ( dm.vict.kr@gmail.com )

let s:Object = g:vim_lib#base#Object#
let s:EventHandle = g:vim_lib#base#EventHandle#
let s:Type = g:vim_lib#base#Type#
let s:Content = g:vim_lib#sys#Content#.new()

"//

""
" Class represents the buffer of editor.
""

let s:Class = s:Object.expand()
let s:Class.properties = {}
let s:Class.properties.name = 'vim_lib#sys#Buffer#'
call s:Class.mix( s:EventHandle )

"//

""
" @property { Dictionary } buffers - A hash of the instances of classes.
" Uses for preventing of duplicating of instances with the same buffer number.
""

let s:Class.buffers = {}

"//

""
" The constructor new() creates object view of a buffer.
"
" @param { Number|String } name - Number or the name of destination buffer. If it is not defined, creates new buffer.
" @throws { Exception } If the provided buffer does not exists.
" @returns { Object } - Returns vim_lib#sys#Buffer# destination buffer.
""

function! s:Class.new( ... )
    if exists( 'a:1' )
        let l:bufnr = s:Type.strIs( a:1 ) ? bufnr( a:1 ) : a:1
        if has_key( self.buffers, l:bufnr )
            return self.buffers[ l:bufnr ]
        endif
    endif

    let l:obj = self.bless()

    let l:obj.number = 0
    if exists( 'a:1' )
        if s:Type.intIs( a:1 )
            if !bufexists( a:1 )
                throw 'IndexOutOfRangeException: Buffer <' . a:1 . '> not found.'
            endif
            let l:obj.number = a:1
        else
            let l:obj.number = bufnr( a:1, 1 )
        endif
    else
        let l:obj.number = bufnr( bufnr( '$' ) + 1, 1 )
    endif

    let l:obj.options = {}
    let l:obj.listenerMap = {}
    let l:obj.listenerAu = {}
    let l:obj.appliedAutocommands = 0

    let self.buffers[ l:obj.getNum() ] = l:obj
    return l:obj
endfunction

"//

""
" Конструктор создает объектное представление текущего буфера.
" @throws IndexOutOfRangeException Выбрасывается при обращении к отсутствующему буферу.
" @return vim_lib#sys#Buffer# Целевой буфер.
""

function! s:Class.current()
    return self.new( bufnr( '%' ) )
endfunction

"//

""
" Static method BufferCurrentNumberGet() returns number of the buffer where the cursor is hold.
"
" @returns { Number } - Returns number of the current buffer.
""

function! s:Class.BufferCurrentNumberGet()
    return bufnr( '%' )
endfunction

"//

""
" Method getNum() returns number of the instance buffer.
"
" @returns { Number } - Returns number of the instance buffer.
""

function! s:Class.getNum()
    return self.number
endfunction

"//

""
" Метод возвращает номер окна, в котором активирован буфер.
" Если буфер активен в более чем одном окне, метод возвращает номер верхнего левого окна.
" @return integer Номер окна, в котором активирован буфер или -1, если буфер не активен.
""

function! s:Class.getWinNum()
    return bufwinnr( self.getNum() )
endfunction

"//

"" {{{
" Метод закрывает все окна, в котором данный буфер является активным, а так же выгружает его из памяти.
" При вызове метода, все несохраненные в буфере данные будут потеряны.
"" }}}
function! s:Class.unload() " {{{
  exe 'bunload! ' . self.getNum()
endfunction " }}}

"//

""
" Метод удаляет вызываемый буфер.
" При вызове метода, все несохраненные в буфере данные будут потеряны.
""
function! s:Class.delete()
  exe 'bw! ' . self.getNum()
  call remove( self.class.buffers, self.getNum() )
endfunction

"//

""
" The private method _setOptions() applies options to current buffer object.
""

function! s:Class._setOptions()
    if self.appliedAutocommands == 0
        let self.appliedAutocommands = 1
        for l:listenerAu in values( self.listenerAu )
            exe l:listenerAu
        endfor
    endif

    for l:listenerMap in values( self.listenerMap )
        exe l:listenerMap
    endfor

    for [ l:option, l:value ] in items( self.options )
        exe 'let &l:' . l:option . ' = "' . l:value . '"'
    endfor
endfunction

"//

""
" Method redraw() redraws content of the buffer.
"
" @returns {} - Returns not a value, redraws the buffer.
""

function! s:Class.redraw()
    if !has_key( self, 'render' )
        throw 'Wrong render'
    endif

    let l:pos = s:Content.pos()

    if self.BufferCurrentNumberGet() != self.getNum()
        call self._bufferDifferentWrite()
    else
        call self._bufferCurrentWrite()
    endif

    call s:Content.pos( l:pos )
endfunction

"//

function! s:Class._bufferDifferentWrite()
    call deletebufline( self.getNum(), 1, '$' )
    let l:Render = self.render
    if s:Type.routineIs( l:Render )
        let l:Render = l:Render()
    endif
    call appendbufline( self.getNum(), 0, l:Render )
endfunction

"//

function! s:Class._bufferCurrentWrite()
    normal ggVGd
    if s:Type.routineIs( self.render )
        silent put = self.render()
        keepjumps 0d
    elseif s:Type.strIs( self.render )
        try
            exe 'silent put = ' . self.render
        catch /.*/
            let self.render = split( self.render, "\n" )
            call append( 0, self.render )
            exe 'normal "_dd'
        endtry
    elseif s:Type.listIs( self.render )
        call append( 0, self.render )
        exe 'normal "_dd'
    endif
endfunction

"//

""
" Method select() makes window of the instance active.
"
" @returns {} - Returns not a value, makes instance window active.
" @throws { Error } If editor has no window buffer.
""

function! s:Class.select()
    let l:winNum = bufwinnr(self.getNum())
    if l:winNum == -1
        throw 'RuntimeException: Buffer <' . self.getNum() . '> not active.'
    endif
    exe l:winNum . 'wincmd w'
endfunction

"//

""
" Static method Select() makes window with buffer {-bufferNumber-} active.
"
" @param { Number } bufferNumber - A number of buffer to activate.
"
" @returns {} - Returns not a value, change active window.
" @throws { Error } If editor has no window with provided buffer.
""

function! s:Class.Select( bufferNumber )
    let l:windowNumber = bufwinnr( a:bufferNumber )
    if l:windowNumber == -1
        throw 'RuntimeException: Buffer <' . a:bufferNumber . '> not active.'
    endif
    exe l:windowNumber . 'wincmd w'
endfunction

"//

""
" Method active() makes buffer active. That is the method applies all listeners if its not settled and redraws
" the content of the buffer.
" To redraw the content the current buffer should have method or field `render`.
""

function! s:Class.active()
    exe 'buffer ' . self.number
    call self._setOptions()
    if has_key( self, 'render' )
        call self.redraw()
    endif
endfunction

"//

function! s:windowResize( dstSize, originalSize, direction )
    if a:dstSize[ -1 : ] == '%'
        exe a:direction . ' resize ' . ( a:originalSize * a:dstSize[ 0 : -2 ] / 100 )
    else
        exe a:direction . ' resize ' . a:dstSize
    endif
endfunction

"//

""
" Method hactive() opens new horizontal window and make it active.
"
" @param { String } pos - Position of new window : above - above current window, below - below current window, top - on top of vim window, bottom - on bottom of vim window.
" @param { Integer } size - Height of new window. Optional parameter. Can accept value in %.
"
" @returns {} - Returns not a value, opens new horizontal window.
""

function! s:Class.hactive( pos, ... )
    let l:winheight = winheight( winnr() )

    let l:positions =
    \{
    \  'above' : 'leftabove',
    \  'top' : 'topleft',
    \  'below' : 'rightbelow',
    \  'bottom' : 'botright'
    \}

    exe 'silent! ' . l:positions[ a:pos ] . ' new'
    if exists( 'a:1' )
        call s:windowResize( a:1, l:winheight, '' )
    endif
    let l:newBufNum = bufnr( '%' )
    call self.active()
    exe 'bw! ' . l:newBufNum
endfunction

"//

""
" Method vactive() opens new vertical window and make it active.
"
" @param { String } pos - Position of new window : before - before current window, after - after current window, left - on left side of vim window, bottom - on right side of vim window.
" @param { Integer } size - Width of new window. Optional parameter. Can accept value in %.
"
" @returns {} - Returns not a value, opens new vertical window.
""

function! s:Class.vactive( pos, ... )
    let l:winwidth = winwidth( winnr() )

    let l:positions =
    \{
    \  'before' : 'leftabove',
    \  'left' : 'topleft',
    \  'after' : 'rightbelow',
    \  'right' : 'botright'
    \}

    exe 'silent! ' . l:positions[ a:pos ] . ' vnew'
    if exists( 'a:1' )
        call s:windowResize( a:1, l:winwidth, 'vertical' )
    endif
    let l:newBufNum = bufnr( '%' )
    call self.active()
    exe 'bw! ' . l:newBufNum
endfunction

"//

""
" Метод определяет локальную опцию буферу.
" Данное значение будет установлено для опции при каждой активации буфера методом active, hactive или vactive.
" @param string name Имя целевой опции.
" @param string value Устанавливаемое значение.
""

function! s:Class.option(name, value)
    let self.options[ a:name ] = a:value
endfunction

"" {{{
" Метод делает буфер временным устанавливая опцию buftype в значение nofile.
"" }}}
function! s:Class.temp() " {{{
  call self.option('buftype', 'nofile')
  call self.option('swapfile', '0')
endfunction " }}}

" Метод listen примеси EventHandle выносится в закрытую область класса.
let s:Class._listen = s:Class.listen
unlet s:Class.listen

"//

""
" Method map() adds mapping for to current buffer.
" The shortcut for insert mode executes in normal mode, after execution method sets insert mode.
" The listener could be a function of current buffer, global function or command to execute.
"
" @param { String } mode - The char that defines mode: 'n' - normal, 'i' - insert, 'v' - visual...
" @param { String } sequence - Shortcut for mapping.
" @param { String } listener - Name of function to execute or command.
"
" @returns {} - Returns not a value, adds mapping.
""

function! s:Class.map( mode, sequence, listener )
    let l:modSeq = substitute( a:sequence, '<', '\\<', '' )
    let l:modSeq = substitute( l:modSeq, '>', '\\>', '' )

    call self._listen( 'keyPress_' . a:mode . a:sequence, a:listener )

    let l:executeHandlers = ' :call ' . self.name . '.current().fire("' . a:mode . '", "' . l:modSeq . '")<CR>'
    if a:mode == 'i'
        let self.listenerMap[ a:mode . a:sequence ] = a:mode . 'noremap <silent> <buffer> ' . a:sequence . ' <Esc>' . l:executeHandlers . a:mode
    else
        let self.listenerMap[ a:mode . a:sequence ] = a:mode . 'noremap <silent> <buffer> ' . a:sequence . l:executeHandlers
    endif
endfunction

"//

""
" Method mapUnlistened() adds mapping to current buffer. The command should be
" declared directly.
"
" @param { String } mode - The letter which defines mode of mapping.
" @param { String } sequence - Keyboard shortcut for mapping.
" @param { String } command - The command with should executes.
"
" @returns {} - Returns not a value, add mapping to current buffer.
""
function! s:Class.mapUnlistened( mode, sequence, command )
    exe a:mode . 'noremap <silent> <buffer> ' . a:sequence . ' ' . a:command
endfunction

"//

""
" Method adds function listener for the buffer, which activates by editor
" event. The listener can be method of current buffer, global function or
" command.
"
" @param { String } events - The name of events that triggered execution. See |autocommand-events|.
" @param { String } listener - The function for execution.
"
" @returns {} - Returns not a value, adds autocommand for buffer.
""
function! s:Class.au( events, listener )
    call self._listen( 'autocmd_' . a:events, a:listener )
    let self.listenerAu[ a:events ] = 'au ' . a:events . ' ' . '<buffer=' . self.getNum() . '> :call ' . self.name . '.current().doau("' . a:events . '")'
endfunction

"//

""
" Method auUnlistened() adds autocommand to current buffer. The command should be a regular command.
"
" @param { String } events - Events for autocommand execution.
" @param { String } command - Commands for execution.
"
" @returns {} - Returns not a value, adds autocommand for buffer.
""
function! s:Class.auUnlistened( events, command )
    exe 'au ' . a:events . ' ' . '<buffer=' . self.getNum() . '> ' .  a:command
endfunction

"//

" Метод ignore примеси EventHandle выносится в закрытую область класса.
let s:Class._ignore = s:Class.ignore
"" {{{
" Метод удаляет функции-обработчики (слушатели) для события клавиатуры.
" @param string mode Режим привязки. Возможно одно из следующих значений: n, v, o, i, l, c.
" @param string sequence Комбинация клавишь, для которой удаляется привязка.
" @param string listener [optional] Имя удаляемой функции-слушателя. Если параметр не задан, удаляются все слушатели данной комбинации клавишь.
"" }}}
function! s:Class.ignoreMap(mode, sequence, ...) " {{{
  if exists('a:1')
    call self._ignore('keyPress_' . a:mode . a:sequence, a:1)
  else
    call self._ignore('keyPress_' . a:mode . a:sequence)
    if has_key(self.listenerMap, a:mode . a:sequence)
      unlet self.listenerMap[a:mode . a:sequence]
    endif
  endif
endfunction " }}}

"" {{{
" Метод удаляет функции-обработчики (слушатели) для события редактора.
" @param string events Имена событий, перечисленных через запятую, для которым отменяется привязка. Доступно одной из приведенных в разделе |autocommand-events| значений.
" @param string listener [optional] Имя удаляемой функции-слушателя. Если параметр не задан, удаляются все слушатели данного события редактора.
"" }}}
function! s:Class.ignoreAu(events, ...)
  if exists('a:1')
    call self._ignore('autocmd_' . a:events, a:1)
  else
    call self._ignore('autocmd_' . a:events)
    if has_key(self.listenerAu, a:events)
      unlet self.listenerAu[a:events]
    endif
  endif
  if len(self.listeners['autocmd_' . a:events]) == 0
    exe 'au! ' . a:events . ' ' . bufname(self.getNum())
  endif
endfunction

" Метод fire примеси EventHandle выносится в закрытую область класса.
let s:Class._fire = s:Class.fire
"" {{{
" Метод генерирует событие клавиатуры для данного буфера.
" @param string mode Режим привязки. Возможно одно из следующих значений: n, v, o, i, l, c.
" @param string sequence Комбинация клавишь, для которой генерируется событие нажатия.
"" }}}

function! s:Class.fire(mode, sequence) " {{{
  call self._fire('keyPress_' . a:mode . a:sequence)
endfunction " }}}

"//

""
" Метод генерирует событие редактора для данного буфера.
" @param string events Имена событий, перечисленных через запятую, для которых выполняется генерация. Доступно одной из приведенных в разделе |autocommand-events| значений.
""

function! s:Class.doau( events )
    call self._fire( 'autocmd_' . a:events )
endfunction

"//

let g:vim_lib#sys#Buffer# = s:Class

