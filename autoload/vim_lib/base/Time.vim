" Author: dmvict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Class = {}

"//

""
" Method new() returns single instance of class { vim_lib#base#Time# }.
"
" @returns { vim_lib#base#Time# } - Returns instance of class.
""

function! s:Class.new()
    return self.singleton
endfunction

"//

""
" Method now() returns the quantity of seconds from 1 January of 1970.
"
" @returns { Number } - Returns the number of seconds from 1 January of 1970.
""

function! s:Class.now()
    return localtime()
endfunction

"//

""
" Method spent() returns the quantity of seconds spent from time {-begin-}.
"
" @returns { Number } - Returns the number of spent seconds.
""

function! s:Class.spent( begin )
    let l:now = self.now()
    return l:now - a:begin
endfunction

"//

""
" @property { vim_lib#base#Path# } singleton - A single exemplar of class.
""

let s:Class.singleton = s:Class

"//

let g:vim_lib#base#Time# = s:Class
