" Date Create: 2015-02-02 17:00:19
" Last Change: see date in git
" Author: Artur Sh. Mamedbekov ( Artur-Mamedbekov@yandex.ru )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )
" Contributor: dmvict ( dm.vict.kr@gmail.com )

let s:Object = g:vim_lib#base#Object#

""
" The mixin adds the mechanism for handling events.
""

let s:Class = s:Object.expand()
let s:Class.properties = {}

""
" @var hash Словарь обработчиков событий, имеющий следующую структуру: {событие: [обработчик, ...], ...}.
""

let s:Class.properties.listeners = {}

"" {{{
" Метод добавляет обработчик события.
" В качестве обработчика события должен выступать метод вызываемого объекта или ссылка на глобальную функцию.
" Позволяется устанавливать несколько обработчиков одного события. В случае наступления события, они будут вызваны в порядке их добавления.
" @param string event Событие.
" @param string|function listener Имя метода-обработчика события или ссылка на функцию-обработчик события.
"" }}}

function! s:Class.listen( event, listener )
    if !has_key( self.listeners, a:event )
        let self.listeners[ a:event ] = []
    endif
    call add( self.listeners[ a:event ], a:listener )
endfunction

""
" Method fire() executes handlers for an event. The method executes hanlers in order defined by
" the order of appending handlers in code.
"
" @param { String } event - The name of event.
"
" @returns {} - Returns not a value, executes handlers for the event.
""

function! s:Class.fire( event )
    if has_key( self.listeners, a:event )
        for l:Listener in self.listeners[a:event]
            if type( l:Listener ) == 2
                call call( l:Listener, [] )
            elseif has_key( self, l:Listener )
                call call( self[ l:Listener ], [], self )
            else
                execute l:Listener
            endif
        endfor
    endif
endfunction

"//

""
" Method ignore() removes event handler.
"
" @param { String } event - Name of the event.
" @param { String|Function } listener - Optional argument. Name of handler ( reference ). If it is not defined method removes all handlers.
"
" @returns { Undefined } - Returns no value, removes handlers.
""

function! s:Class.ignore( event, ... )
    if !exists( 'a:1' )
        let self.listeners[ a:event ] = []
    else
        let l:pos = index( self.listeners[ a:event ], a:1 )
        while l:pos != -1
            call remove( self.listeners[ a:event ], l:pos )
            let l:pos = index( self.listeners[ a:event ], a:1 )
        endwhile
    endif
endfunction

"//

let g:vim_lib#base#EventHandle# = s:Class
