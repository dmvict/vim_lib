" Author: dmvict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Object = g:vim_lib#base#Object#
let s:Class = s:Object.expand()

"//

""
" @prop { String } slash - The property keeps the paths delimeter of the platform.
""

let s:Class.slash = ( has( 'win16' ) || has( 'win32' ) || has( 'win64' ) || has( 'win95' ) ) ? '\' : '/'

"//

""
" Method new() returns single instance of class { vim_lib#base#Path# }.
"
" @returns { vim_lib#base#Path# } - Returns instance of class.
""

function! s:Class.new()
    return self.singleton
endfunction

"//

""
" Method isAbsolute() checks weather the passed path {-path-} is absolute path.
"
" @param { String } path - Path to check.
"
" @returns { Boolean } - Returns true if path is absolute, otherwise, returns false.
""

function! s:Class.isAbsolute( path )
    if self.slash == '/'
        if a:path[ 0 ] == self.slash
            return 1
        else
            return 0
        endif
    else
        if matchstr( a:path, '^\w:\\' ) != ''
            return 1
        else
            return 0
        endif
    endif
endfunction

"//

""
" Method isRelative() checks weather the passed path {-path-} is relative path.
"
" @param { String } path - Path to check.
"
" @returns { Boolean } - Returns true if path is relative, otherwise, returns false.
""

function! s:Class.isRelative( path )
    return !self.isAbsolute( a:path )
endfunction

"//

""
" Method dir() gets the parent directory from path {-path-}.
"
" @param { String } path - Path to get directory.
"
" @returns { String } - Returns path to parent directory.
""

function! s:Class.dir( path )
    return fnamemodify( a:path, ':p:h' )
endfunction

"//

""
" Method name() gets name of file from path {-path-}.
"
" @param { String } path - Path to get name.
"
" @returns { String } - Returns name of file.
""

function! s:Class.name( path )
    return fnamemodify( a:path, ':t' )
endfunction

"//

""
" Method join() concatenates paths passed in arguments.
"
" @param { String } path - Path to concatenate with other paths.
" @param { String } ... - Paths to concatenate with first path.
"
" @returns { String } - Returns path to parent directory.
" @throw { Error } If arguments length is less than 1.
" @throw { Error } If {-path-} is not a String.
" @throw { Error } If exists several absolute paths or absolute path appends to result.
""

function! s:Class.join( path, ... )
    if type( a:path ) != 1
        throw 'Expects string {-path-}'
    endif

    let l:result = a:path

    for l:path in a:000
        if self.isAbsolute( l:path )
            throw 'Absolute path should be passed at first argument'
        endif
        let l:result = l:result . self.slash . l:path
    endfor

    return l:result
endfunction

"//

""
" Method current() returns current working directory where method is called.
"
" @returns { String } - Returns current working directory.
" @throw { Error } If arguments are provided.
""

function! s:Class.current()
    return getcwd()
endfunction

"//

""
" @property { vim_lib#base#Path# } singleton - A single exemplar of class.
""

let s:Class.singleton = s:Class.bless()

"//

let g:vim_lib#base#Path# = s:Class
