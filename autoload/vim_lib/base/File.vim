" Date Create: 2015-01-12 23:45:01
" Last Change: see date in git
" Author: Artur Sh. Mamedbekov ( Artur-Mamedbekov@yandex.ru )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )
" Contributor: dmvict ( dm.vict.kr@gmail.com )

let s:Object = g:vim_lib#base#Object#
let s:Path = g:vim_lib#base#Path#

let s:Class = s:Object.expand()

"//

"" {{{
" Конструктор создает объектное представление файла.
" @param string address Относительный адрес файла. Основой адреса является текущий файл редактора.
" @return vim_lib#base#File# Целевой файл.
"" }}}

function! s:Class.relative(address)
  let l:obj = self.bless()
  let l:obj.address = fnamemodify(expand('%'), ':p:h') . '/' . a:address
  return l:obj
endfunction

"//

"" {{{
" Конструктор создает объектное представление файла.
" @param string Абсолютный адрес файла.
" @return vim_lib#base#File# Целевой файл.
"" }}}

function! s:Class.absolute(address) " {{{
  let l:obj = self.bless()
  let l:obj.address = expand(a:address)
  return l:obj
endfunction " }}}

"//

"" {{{
" Метод возвращает адрес файла. Адрес каталогов не завершается слешем.
" @return string Адрес файла.
"" }}}

function! s:Class.getAddress() " {{{
  return self.address
endfunction " }}}

"//

""
" Method getName() returns name of file
"
" @return { String } - Returns name of file.
""

function! s:Class.getName()
    return s:Path.name( self.getAddress() )
endfunction

"//

""
" Method getDir() returns directory of file
"
" @return { String } - Returns directory of file.
""

function! s:Class.getDir()
  return self.class.absolute( s:Path.dir( self.getAddress() ) )
endfunction

"//

"" {{{
" Метод возвращает файл, предположительно содержащийся в данном каталоге.
" @param string name Имя целевого файла.
" @return vim_lib#base#File# Целевой файл.
"" }}}

function! s:Class.getChild(name) " {{{
  return self.class.absolute( s:Path.join( self.getAddress(), a:name ) )
endfunction " }}}

"//

"" {{{
" Метод возвращает массив имен файлов, содержащихся в данном каталоге, за исключением файлов . и ..
" @return array Массив имен файлов, содержащихся в данном каталоге.
"" }}}

function! s:Class.getChildren() " {{{
  let l:address = self.getAddress()
  let l:childAddress = split(globpath(l:address, '*') . "\n" . globpath(l:address, '.*'), "\n")
  let l:childNames = []
  for l:file in l:childAddress
    if l:file !~# '\/\.\.\/\?$' && l:file !~# '\/\.\/\?$'
      call add(l:childNames, fnamemodify(l:file, ':t'))
    endif
  endfor
  return l:childNames
endfunction " }}}

"//

"" {{{
" Метод создает файл, если он еще не был создан.
"" }}}

function! s:Class.createFile() " {{{
  call writefile([''], self.getAddress())
endfunction " }}}

"//

"" {{{
" Метод создает каталог, если он еще не был создан.
"" }}}

function! s:Class.createDir() " {{{
  if !self.isExists()
    call mkdir(self.getAddress(), 'p')
  endif
endfunction " }}}

"//

"" {{{
" Метод удаляет файл.
"" }}}

function! s:Class.deleteFile() " {{{
  call delete(self.getAddress())
endfunction " }}}

"//

"" {{{
" Метод удаляет каталог.
"" }}}

function! s:Class.deleteDir() " {{{
  if has("win16") || has("win32") || has("win64")
    call system('rmdir /s /q ' . self.getAddress())
  else
    call system('rm -rf ' . self.getAddress())
  endif
endfunction " }}}

"//

"" {{{
" Метод считывает содержимое файла в массив.
" @return array Массив строк, содержащихся в файле.
"" }}}

function! s:Class.read() " {{{
  return readfile(self.getAddress())
endfunction " }}}

"//

""
" Method write() appends strings to file. The method can append list of strings
" and single string.
"
" @param { String|List } strs - Strings to append to file.
"
" @return {} - Returns not a value, appends strings to file.
""

function! s:Class.write( strs )
    if type( a:strs ) == 3
        call self.rewrite( self.read() + a:strs )
    else
        call self.rewrite( add( self.read(), a:strs ) )
    endif
endfunction

"//

""
" Method rewrite() file by provided content. The method can write list of strings
" and single string.
"
" @param { String|List } strs - Content for the file.
"
" @return {} - Returns not a value, rewrites file by provided content.
""

function! s:Class.rewrite( strs )
    if type( a:strs ) == 3
        call writefile( a:strs, self.getAddress() )
    else
        call writefile( [ a:strs ], self.getAddress() )
    endif
endfunction

"//

"" {{{
" Метод определяет, является ли данный компонент файловой системы файлом.
" @return bool true - если это файл, иначе - false.
"" }}}

function! s:Class.isFile() " {{{
  return !isdirectory(self.getAddress())
endfunction " }}}

"//

"" {{{
" Метод определяет, является ли данный компонент файловой системы каталогом.
" @return bool true - если это каталог, иначе - false.
"" }}}

function! s:Class.isDir() " {{{
  return isdirectory(self.getAddress())
endfunction " }}}

"//

"" {{{
" Метод определяет, существует ли данный файл, или файл с данным именем в каталоге.
" @param string name [optional] Искомый в данном каталоге файл. Если параметр не задан, метод оценивает существование файла вызываемого объекта.
" @return bool true - если файл существует, иначе - false.
"" }}}
function! s:Class.isExists(...) " {{{
  if exists('a:1')
    return empty(glob(self.getAddress() . '/' . a:1))? 0 : 1
  else
    return empty(glob(self.getAddress()))? 0 : 1
  endif
endfunction " }}}

let g:vim_lib#base#File# = s:Class
