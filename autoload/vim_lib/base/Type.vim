" Author: dmvict ( dm.vict.kr@gmail.com )
" License: GNU GPL v3 ( http://www.gnu.org/copyleft/gpl.html )

let s:Class = {}

"//

""
" Method new() returns single instance of class { vim_lib#base#Time# }.
"
" @returns { vim_lib#base#Time# } - Returns instance of class.
""

function! s:Class.new()
    return self.singleton
endfunction

"//

""
" Method strIs() checks whether the value {-src-} is a String.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a String, otherwise, returns false.
""

function! s:Class.strIs( src )
    return type( a:src ) == 1
endfunction

"//

""
" Method routineIs() checks whether the value {-src-} is a Funcref.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Funcref, otherwise, returns false.
""

function! s:Class.routineIs( src )
    return type( a:src ) == 2
endfunction

"//

""
" Method listIs() checks whether the value {-src-} is a List.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a List, otherwise, returns false.
""

function! s:Class.listIs( src )
    return type( a:src ) == 3
endfunction

"//

""
" Method mapIs() checks whether the value {-src-} is a Dictionary.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Dictionary, otherwise, returns false.
""

function! s:Class.mapIs( src )
    return type( a:src ) == 4
endfunction

"//

""
" Method intIs() checks whether the value {-src-} is an Integer.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is an Integer, otherwise, returns false.
""

function! s:Class.intIs( src )
    return type( a:src ) == 0
endfunction

"//

""
" Method floatIs() checks whether the value {-src-} is a Float.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Float, otherwise, returns false.
""

function! s:Class.floatIs( src )
    return type( a:src ) == 5
endfunction

"//

""
" Method numberIs() checks whether the value {-src-} is a Number.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Number, otherwise, returns false.
""

function! s:Class.numberIs( src )
    return self.intIs( a:src ) || self.floatIs( a:src )
endfunction

"//

""
" Method boolIs() checks whether the value {-src-} is a Boolean.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Boolean, otherwise, returns false.
""

function! s:Class.boolIs( src )
    return type( a:src ) == 6
endfunction

"//

""
" Method boolLikeIs() checks whether the value {-src-} is a BoolLike.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a BoolLike, otherwise, returns false.
""

function! s:Class.boolLikeIs( src )
    return type( a:src ) == 6 || ( self.numberIs( a:src ) && a:src == 0 ) || ( self.numberIs( a:src ) && a:src == 1 )
endfunction

"//

""
" Method noneIs() checks whether the value {-src-} is a None.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a None, otherwise, returns false.
""

function! s:Class.noneIs( src )
    return type( a:src ) == 7
endfunction

"//

""
" Method jobIs() checks whether the value {-src-} is a Job.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Job, otherwise, returns false.
""

function! s:Class.jobIs( src )
    return type( a:src ) == 8
endfunction

"//

""
" Method channelIs() checks whether the value {-src-} is a Channel.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Channel, otherwise, returns false.
""

function! s:Class.channelIs( src )
    return type( a:src ) == 9
endfunction

"//

""
" Method blobIs() checks whether the value {-src-} is a Blob.
"
" @param { * } src - A value to check.
"
" @returns { BoolLike } - Returns true if the value is a Blob, otherwise, returns false.
""

function! s:Class.blobIs( src )
    return type( a:src ) == 10
endfunction

"//

""
" Method typeStrGet() returns type of argument {-src-} in string format.
"
" @param { * } src - A value to get type.
"
" @returns { String } - Returns type of argument in string format.
""

function! s:Class.typeStrGet( src )
    let l:typesMap =
    \{
    \   '0'  : 'Integer',
    \   '5'  : 'Double',
    \   '1'  : 'String',
    \   '2'  : 'Function',
    \   '3'  : 'List',
    \   '4'  : 'Dictionary',
    \   '6'  : 'Boolean',
    \   '7'  : 'None',
    \   '8'  : 'Job',
    \   '9'  : 'Channel',
    \   '10' : 'Blob',
    \}
    return l:typesMap[ type( a:src ) ]
endfunction

"//

""
" @property { vim_lib#base#Type# } singleton - A single exemplar of class.
""

let s:Class.singleton = s:Class

"//

let g:vim_lib#base#Type# = s:Class
